# A Dockerfile for MediaGoblin hacking.
#
# See Dockerfile-debian-python3-sqlite for documentation.

FROM debian:buster

RUN apt-get update && apt-get install -y \
automake \
git \
nodejs \
npm \
python-dev \
virtualenv

RUN apt-get install -y \
python-alembic \
python-celery \
python-jsonschema \
python-kombu \
python-lxml \
python-migrate  \
python-mock \
python-py \
python-pytest \
python-pytest-xdist \
python-six \
python-sphinx \
python-webtest

RUN apt-get install -y \
gstreamer1.0-libav \
gstreamer1.0-plugins-bad \
gstreamer1.0-plugins-base \
gstreamer1.0-plugins-good \
gstreamer1.0-plugins-ugly \
libsndfile1-dev \
python-gst-1.0 \
python-numpy \
python-scipy

RUN apt-get install -y \
gir1.2-gst-plugins-base-1.0 \
gir1.2-gstreamer-1.0 \
gstreamer1.0-tools \
python-gi

RUN mkdir /opt/mediagoblin
RUN chown -R www-data:www-data /opt/mediagoblin
WORKDIR /opt/mediagoblin

RUN mkdir /var/www
RUN chown root:www-data /var/www
RUN chmod g+w /var/www

RUN groupadd --system mediagoblin --gid 1024 && adduser www-data mediagoblin

USER www-data

RUN git clone --depth=1 git://git.savannah.gnu.org/mediagoblin.git -b master .
RUN git submodule init && git submodule update

RUN ./bootstrap.sh
RUN VIRTUALENV_FLAGS='--system-site-packages' ./configure
RUN make

RUN ./bin/pip install scikits.audiolab

RUN echo '[[mediagoblin.media_types.audio]]' >> mediagoblin.ini
RUN echo '[[mediagoblin.media_types.video]]' >> mediagoblin.ini

RUN ./bin/gmg dbupdate
RUN ./bin/gmg adduser --username admin --password a --email admin@example.com
RUN ./bin/gmg makeadmin admin

EXPOSE 6543/tcp

CMD ["./lazyserver.sh", "--server-name=broadcast"]
